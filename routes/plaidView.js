"use strict";

const express = require('express');
const plaidLinkViewRoutes = express.Router();

module.exports = function(dbHelpers) {

	plaidLinkViewRoutes.get("/link/:_puid/:_mcat", (req, res) => {
		dbHelpers.verifyUserCredentials(req.params._puid, req.params._mcat, (error, result) => {
			if (error) {
				console.log(error.message)
				res.render("errors/unauthorized")
			} else {
				console.log(result)
				if (result) {
					let templateVars = {_puid: req.params._puid, _mcat: req.params._mcat }
					res.render("link_card", templateVars)
				} else {
					res.render("errors/unauthorized")
				}
			}
		});
	});

	plaidLinkViewRoutes.get("/errors/internal/:_puid/:_mcat", (req, res) => {
		let templateVars = {_puid: req.params._puid, _mcat: req.params._mcat}
		res.render("errors/internal", templateVars)
	});

	return plaidLinkViewRoutes;
}