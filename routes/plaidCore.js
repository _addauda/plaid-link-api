"use strict";

const express = require('express');
const plaidRoutes = express.Router();
const Validator = require('node-input-validator');

module.exports = function(client, dbHelpers) {

	const generatePlaidItem = (item_id, public_key, access_token, institution, _puid) => {
		let plaidItem = []
		plaidItem.push(item_id);
		plaidItem.push(public_key);
		plaidItem.push(access_token);
		plaidItem.push(institution.name);
		plaidItem.push(institution.institution_id);
		plaidItem.push(_puid);
		return plaidItem;
	}

	const generatePlaidAccounts = (item_id, accounts) => {
		let plaidAccounts = []
		for (let account of accounts) {
			console.log("Account", account)
			let accountArray = Object.values(account)
			accountArray.push(item_id)
			plaidAccounts.push(accountArray)
		}
		return plaidAccounts;
	}

	plaidRoutes.post('/accessToken', function(req, res) {

		let validator = new Validator(req.body,
			{
				'link_session_id': 'required|string',
				'institution': 'required|object',
				'institution.name': 'required|string',
				'institution.institution_id': 'required|string',
				'accounts': 'required|array',
				'account.*': 'required|string',
				'_puid': 'required|string',
				'_mcat': 'required|string',
			}
		);

		validator.check().then((matched) => {
			if(!matched) {
				console.log("MATCH ERROR",matched)
				res.render("errors/unauthorized")
			}
			else {
				dbHelpers.verifyUserCredentials(req.body._puid, req.body._mcat, (error, result) => {
					if (error) {
						console.log(error.message)
						res.redirect(`/services/plaid/view/errors/internal/${req.body._puid}/${req.body._mcat}`)
					} else {
						if (result) {
							client.exchangePublicToken(req.body.public_token, function(error, tokenResponse) {
								if (error) {
									console.log('Could not exchange public_token!' + '\n' + error);
									res.redirect(`/services/plaid/view/errors/internal/${req.body._puid}/${req.body._mcat}`)
								}
				
								let plaidItem = generatePlaidItem(tokenResponse.item_id, req.body.public_token, tokenResponse.access_token, req.body.institution, req.body._puid);
								
								let plaidAccounts = generatePlaidAccounts(tokenResponse.item_id,req.body.accounts);
				
								console.log("plaidItem", plaidItem);
								console.log("plaidAccounts", plaidAccounts);
				
								dbHelpers.createItem(plaidItem, (error, result) => {
									if (error) {
										console.log(error.message)
										res.sendStatus(500);
									} else {
										dbHelpers.createAccounts(plaidAccounts, (error, result) => {
											if (error) {
												console.log(error.message)
												res.redirect(`/services/plaid/view/errors/internal/${req.body._puid}/${req.body._mcat}`)
											} else {
												let templateVars = {accounts: plaidAccounts, institution: plaidItem[3]};
												res.render("link_success", templateVars)
											}
										});
									}
								});
							});
						} else {
							res.render("errors/unauthorized")
						}
					}
				});
			}
		});
	});

	return plaidRoutes;
}