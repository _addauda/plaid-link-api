"use strict";

const express = require('express');
const userRoutes = express.Router();

module.exports = function(dbHelpers) {

	const generateUser = (_puid, first_name, last_name, gender) => {
		let user = []
		user.push(_puid);
		user.push(first_name);
		user.push(last_name);
		user.push(gender);
		return user;
	}

	userRoutes.post("/create", function(req, res) {
		let user = generateUser(req.body.id, req.body.first_name, req.body.last_name, req.body.gender)
		dbHelpers.createUser(user, (error, result) => {
			if (error) {
				console.log(error.message)
				res.sendStatus(500);
			} else {
				res.sendStatus(201);
			}
		});
	});

	return userRoutes;
}