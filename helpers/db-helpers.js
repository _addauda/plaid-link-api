"use strict";
const format = require('pg-format');

// Defines helper functions for saving and getting tweets, using the database `db`
module.exports = function makeDBHelpers(db) {
	return {
		createUser: (user, callback) => {
			let queryString = format("INSERT INTO users (id, first_name, last_name, gender) VALUES ($1, $2, $3, $4)")
			db.query(queryString, user, (error, result) => {
				if (error) {
					callback(error, false)
				}
				callback(null, result)
			})
		},

		createItem: (plaidItem, callback) => {
			let queryString = format("INSERT INTO items (id, public_key, access_token, institution_name, institution_id, user_id) VALUES ($1, $2, $3, $4, $5, $6)")
			db.query(queryString, plaidItem, (error, result) => {
				if (error) {
					callback(error, false)
				}
				callback(null, result)
			})
		},

		createAccounts: (plaidAccounts, callback) => {
			let queryString = format("INSERT INTO accounts (id, name, mask, type, subtype, item_id) VALUES %L", plaidAccounts)
			db.query(queryString, (error, result) => {
				if (error) {
					callback(error, false)
				}
				callback(null, result)
			})
		},

		verifyUserCredentials: (_puid, _mcat, callback) => {
			let queryString = format("SELECT id FROM users WHERE id = %L AND many_chat_auth_token = %L LIMIT 1", _puid, _mcat)
			db.query(queryString, (error, result) => {
				if (error) {
					callback(error, false)
				}
				else if (result.rowCount === 1) {
					callback(null, true)
				}
				else {
					callback(null, false)
				}
			})
		}
	};
}