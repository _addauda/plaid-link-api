'use strict';
require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');

const plaid = require('plaid');
const plaidClient = new plaid.Client(
	process.env.PLAID_CLIENT_ID,
	process.env.PLAID_SECRET,
	process.env.PLAID_PUBLIC_KEY,
	plaid.environments.sandbox
);

const Pool = require('pg').Pool
const pool = new Pool({
	host: process.env.DB_HOST,
	database: process.env.DB_NAME,
	user: process.env.DB_USER,
	password: process.env.DB_PASSWORD,
	port: process.env.DB_PORT,
});

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
//app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));

app.set("view engine", "ejs");


const dbHelpers = require("./helpers/db-helpers.js")(pool);

// Routes
const userRoutes = require("./routes/users.js")(dbHelpers);
const plaidViewRoutes = require("./routes/plaidView.js")(dbHelpers);
const plaidCoreRoutes = require("./routes/plaidCore.js")(plaidClient, dbHelpers);

app.use("/users", userRoutes);
app.use("/services/plaid/core", plaidCoreRoutes);
app.use("/services/plaid/view", plaidViewRoutes);


app.get('/', (req, res) => {
	res.send({status:"Success", message:"Endpoint is up"});
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);